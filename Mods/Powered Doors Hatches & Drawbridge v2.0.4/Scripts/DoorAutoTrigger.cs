using System;
using UnityEngine;


public class DoorAutoTrigger : MonoBehaviour
{
	public Vector3i blockPos;
	public int cIdx;
	public BlockEntityData ebcd;
	public BlockValue blockValue;
	public bool normallyOpened;
	WorldBase world;
	
	
	void Update()
	{
		if(world == null)
		{
			world = GameManager.Instance.World;
		}
		if(blockPos == default(Vector3i))
		{
			BlockPoweredDoor.DebugMsg("blockPos is default");
			return;
		}
		Block block = Block.list[blockValue.type];
		Type blockType = Block.list[blockValue.type].GetType();
		bool hasPower = false;
		if(blockType == typeof(BlockPoweredDoor))
		{
			BlockPoweredDoor blockPoweredDoor = block as BlockPoweredDoor;
			hasPower = BlockPoweredDoor.HasActivePower(world, cIdx, blockPos);
		}
		if(blockType == typeof(BlockPoweredDrawBridge))
		{
			BlockPoweredDrawBridge blockPoweredDrawBridge = block as BlockPoweredDrawBridge;
			hasPower = BlockPoweredDrawBridge.HasActivePower(world, cIdx, blockPos);
		}
		if(normallyOpened)
		{
			BlockPoweredDoor.DebugMsg("normallyOpened");
			if(hasPower)
			{
				BlockPoweredDoor.DebugMsg("normallyOpened & HasActivePower");
				if(BlockDoor.IsDoorOpen(blockValue.meta))
				{
					BlockPoweredDoor.DebugMsg("normallyOpened & HasActivePower & isOpen");
					// Door is opened, close it.
					PlayAnimation(true);
				}
			}
			else
			{
				BlockPoweredDoor.DebugMsg("normallyOpened & Has no ActivePower");
				if(!BlockDoor.IsDoorOpen(blockValue.meta))
				{
					BlockPoweredDoor.DebugMsg("normallyOpened & Has no ActivePower & is closed");
					// Door is closed, open it.
					PlayAnimation(false);
				}
			}
		}
		else
		{
			BlockPoweredDoor.DebugMsg("normal dooor");
			if(hasPower)
			{
				
				BlockPoweredDoor.DebugMsg("Power is on");
				// Power is on
				if(!BlockDoor.IsDoorOpen(blockValue.meta))
				{
					BlockPoweredDoor.DebugMsg("Door is closed, open it.");
					// Door is closed, open it.
					PlayAnimation(false);
				}
				else
				BlockPoweredDoor.DebugMsg("Door is already open, no action taken.");
			}
			else
			{
				BlockPoweredDoor.DebugMsg("Power is off");
				// Power is off
				if(BlockDoor.IsDoorOpen(blockValue.meta))
				{
					BlockPoweredDoor.DebugMsg("Door is opened, close it.");
					// Door is opened, close it.
					PlayAnimation(true);
				}
				else
				BlockPoweredDoor.DebugMsg("Door is already closed, no action taken.");
			}
		}
	}
	
	public void PlayAnimation(bool isOpen)
	{
		BlockPoweredDoor.DebugMsg("PlayAnimation running");
		Block block = Block.list[blockValue.type];
		if(block == null)
		{
			BlockPoweredDoor.DebugMsg("Block is null, cant play animation");
		}
		Type blockType = Block.list[blockValue.type].GetType();
		if(blockType == typeof(BlockPoweredDoor) || blockType == typeof(BlockPoweredDrawBridge))
		{
			if(block != null)
			{
				string openSound = "";
				string closeSound = "";
				if (block.Properties.Values.ContainsKey("OpenSound"))
				{
					openSound = block.Properties.Values["OpenSound"];
				}
				if (block.Properties.Values.ContainsKey("CloseSound"))
				{
					closeSound = block.Properties.Values["CloseSound"];
				}
				bool hasPower = false;
				Animator doorAnimator = null;
				if(blockType == typeof(BlockPoweredDoor))
				{
					BlockPoweredDoor blockPoweredDoor = block as BlockPoweredDoor;
					doorAnimator = blockPoweredDoor.DoorAnimator(ebcd);
					hasPower = BlockPoweredDoor.HasActivePower(world, cIdx, blockPos);
				}
				if(blockType == typeof(BlockPoweredDrawBridge))
				{
					BlockPoweredDrawBridge blockPoweredDrawBridge = block as BlockPoweredDrawBridge;
					doorAnimator = blockPoweredDrawBridge.DoorAnimator(ebcd);
					hasPower = BlockPoweredDrawBridge.HasActivePower(world, cIdx, blockPos);
				}
				if(doorAnimator == null)
				{
					BlockPoweredDoor.DebugMsg("doorAnimator null");
				}
				if(doorAnimator != null)
				{
					bool flag = false;
					if(hasPower)
					{
						flag = true;
					}
					if (flag)
					{
						//Open sound
						Audio.Manager.BroadcastPlayByLocalPlayer(blockPos.ToVector3() + Vector3.one * 0.5f, openSound);
					}
					else
					{
						//close sound
						Audio.Manager.BroadcastPlayByLocalPlayer(blockPos.ToVector3() + Vector3.one * 0.5f, closeSound);
					}
					doorAnimator.SetBool("IsOpen", flag);
					doorAnimator.SetTrigger("OpenTrigger");
					ChunkCluster chunkCluster = world.ChunkClusters[cIdx];
					if (chunkCluster == null)
					{
						return;
					}
					bool _bOpen = !BlockDoor.IsDoorOpen(blockValue.meta);
					blockValue.meta = (byte)(((!_bOpen) ? 0 : 1) | ((int)blockValue.meta & -2));
					if (!world.IsRemote())
					{
						world.SetBlockRPC(cIdx, blockPos, blockValue);
					}
					else
					{
						chunkCluster.SetBlockRaw(blockPos, blockValue);
					}
				}
				else
				BlockPoweredDoor.DebugMsg("doorAnimator is null (ActivateBlock)");
			}	
		}
	}
	
}
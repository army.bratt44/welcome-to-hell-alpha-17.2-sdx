using UnityEngine;
using System;
using System.Collections.Generic;

public class MultiLevelElevatorController : MonoBehaviour
{
	public BlockEntityData ebcd;
	public BlockMultiLevelElevatorGround blockMultiLevelElevatorGround;
	public Vector3 startPos;
	public Vector3 endPos;
	public bool moveElevator;
	private float journeyLength;
	public float startTime;
	public float upDownSpeed;
	public Transform liftPlatform;
	public Vector3i groundBlockPos;
	public BlockValue groundBlockValue;
	public AudioSource motorSound;
	WorldBase worldBase = GameManager.Instance.World;
	public int selectedFloor;
	
	public Vector3 vehicleStartPos;
	public Vector3 vehicleEndPos;
	private float  vehicleJourneyLength;
	public float vehicleStartTime;
	public float vehicleUpDownSpeed;
	
	private bool openDoors;
	private float doorJourneyLength;
	public float doorStartTime;
	public float doorSpeed;
	public bool closeDoors;
	public bool hasDoors;
	public bool twoDoors;
	
	public Vector3i targetPos3i;
	public Vector3i startPos3i;
	
	public bool hasShaftDoors;
	public bool twoShaftDoors;
	public bool closeShaftDoors;
	public bool openShaftDoors;
	public float shaftDoorSpeed;
	public float shaftDoorStartTime;
	private float shaftDoorJourneyLength;
	public Transform leftShaftDoorTransform;
	public Transform rightShaftDoorTransform;
	public Vector3 leftShaftStartPos;
	public Vector3 leftShaftEndPos;
	public Vector3 rightShaftStartPos;
	public Vector3 rightShaftEndPos;
	public static List<Vector3> shaftDoorStartPositions = new List<Vector3>();
	public static List<Vector3> shaftDoorEndPositions = new List<Vector3>();
	
	float shaftDoorStartX;
	float shaftDoorStartZ;
	float shaftDoorEndX;
	float shaftDoorEndZ;
	
	public void SetShaftDoorTransforms(Vector3i _blockPos, bool _openDoor)
	{
		bool foundDoor = false;
		Vector3i newBlockPos = _blockPos;
		if(newBlockPos != null && newBlockPos != default(Vector3i))
		{
			BlockValue newBlockValue = worldBase.GetBlock(newBlockPos);
			Block newBlock = Block.list[newBlockValue.type];
			Type newType = Block.list[newBlockValue.type].GetType();
			if(newType == typeof(BlockMultiLevelElevatorGround))
			{
				BlockMultiLevelElevatorGround elevatorBlock = newBlock as BlockMultiLevelElevatorGround;
				if(elevatorBlock != null)
				{
					leftShaftStartPos = elevatorBlock.leftShaftStartPos;
					leftShaftEndPos = elevatorBlock.leftShaftEndPos;
					rightShaftStartPos = elevatorBlock.rightShaftStartPos;
					rightShaftEndPos = elevatorBlock.rightShaftEndPos;
				}
			}
			if(newType == typeof(BlockMultiLevelElevatorFloor))
			{
				BlockMultiLevelElevatorFloor elevatorBlock = newBlock as BlockMultiLevelElevatorFloor;
				if(elevatorBlock != null)
				{
					leftShaftStartPos = elevatorBlock.leftShaftStartPos;
					leftShaftEndPos = elevatorBlock.leftShaftEndPos;
					rightShaftStartPos = elevatorBlock.rightShaftStartPos;
					rightShaftEndPos = elevatorBlock.rightShaftEndPos;
				}
			}
			BlockEntityData newEbcd = worldBase.ChunkClusters[0].GetBlockEntity(newBlockPos);
			if(newEbcd != null && newEbcd.bHasTransform)
			{
				leftShaftDoorTransform = BlockMultiLevelElevatorGround.FindTransformNamed(newEbcd, BlockMultiLevelElevatorGround.ShaftDoor1Transform(newBlock));
				if(leftDoorTransform != null && leftDoorTransform != default(Transform))
				{
					foundDoor = true;
				}
				rightShaftDoorTransform = BlockMultiLevelElevatorGround.FindTransformNamed(newEbcd, BlockMultiLevelElevatorGround.ShaftDoor2Transform(newBlock));
				if(rightShaftDoorTransform != null || rightShaftDoorTransform != default(Transform))
				{
					foundDoor = true;
				}
			}
		}
		if(foundDoor)
		{
			shaftDoorSpeed = doorSpeed;
			shaftDoorStartTime = Time.time;
			if(_openDoor)
			{
				openShaftDoors = true;
				return;
			}
			else
				closeShaftDoors = true;
		}
		return;
	}
	
	public void CloseShaftDoors()
	{
		shaftDoorJourneyLength = Vector3.Distance(leftShaftEndPos, leftShaftStartPos);
		float shaftDoorDistCovered = (Time.time - shaftDoorStartTime) * shaftDoorSpeed;
		float shaftDoorFracJourney = shaftDoorDistCovered / shaftDoorJourneyLength;
		leftShaftDoorTransform.localPosition = Vector3.Lerp(leftShaftStartPos, leftShaftEndPos, shaftDoorFracJourney);
		if(twoShaftDoors)
		{
			rightShaftDoorTransform.localPosition = Vector3.Lerp(rightShaftStartPos, rightShaftEndPos, shaftDoorFracJourney);
		}
		if(leftShaftDoorTransform.localPosition == leftShaftEndPos)
		{
			shaftDoorStartTime = Time.time;
			closeShaftDoors = false;
		}
	}
	
	public void OpenShaftDoors()
	{
		shaftDoorJourneyLength = Vector3.Distance(leftShaftStartPos, leftShaftEndPos);
		float shaftDoorDistCovered = (Time.time - shaftDoorStartTime) * shaftDoorSpeed;
		float shaftDoorFracJourney = shaftDoorDistCovered / shaftDoorJourneyLength;
		leftShaftDoorTransform.localPosition = Vector3.Lerp(leftShaftEndPos, leftShaftStartPos, shaftDoorFracJourney);
		if(twoShaftDoors)
		{
			rightShaftDoorTransform.localPosition = Vector3.Lerp(rightShaftEndPos, rightShaftStartPos, shaftDoorFracJourney);
		}
		if(leftShaftDoorTransform.localPosition == leftShaftStartPos)
		{
			shaftDoorStartTime = Time.time;
			openShaftDoors = false;
		}
	}
	
	public static bool IsAtThisFloor(byte _metadata)
	{
		return ((int)_metadata & 1 << 1) != 0;
	}
	
	void FixedUpdate()
	{
		if(moveElevator)
		{
			MoveToFloor();
		}
		if(closeDoors)
		{
			CloseDoors();
		}
		if(openDoors)
		{
			OpenDoors();
		}
		if(closeShaftDoors)
		{
			CloseShaftDoors();
		}
		
		if(openShaftDoors)
		{
			OpenShaftDoors();
		}
	}
	public Transform leftDoorTransform;
	public Transform rightDoorTransform;
	public Vector3 leftStartPos;
	public Vector3 leftEndPos;
	public Vector3 rightStartPos;
	public Vector3 rightEndPos;
	
	
	
	public void CloseDoors()
	{
		doorJourneyLength = Vector3.Distance(leftEndPos, leftStartPos);
		float doorDistCovered = (Time.time - doorStartTime) * doorSpeed;
		float doorFracJourney = doorDistCovered / doorJourneyLength;
		if(leftDoorTransform.localPosition == leftStartPos)
		{
			BlockMultiLevelElevatorGround.PlayDoorSound(groundBlockPos, false);
			bool goingUp = BlockMultiLevelElevatorGround.GoingUp(startPos.y, endPos.y);
			BlockMultiLevelElevatorGround.TriggerAllArrows(worldBase, groundBlockPos, goingUp, false);
		}
		leftDoorTransform.localPosition = Vector3.Lerp(leftStartPos, leftEndPos, doorFracJourney);
		if(twoDoors)
		{
			rightDoorTransform.localPosition = Vector3.Lerp(rightStartPos, rightEndPos, doorFracJourney);
		}
		if(leftDoorTransform.localPosition == leftEndPos)
		{
			// BlockMultiLevelElevatorGround.DebugMsg("Doors closed");
			startTime = Time.time;
			moveElevator = true;
			if(blockMultiLevelElevatorGround != null)
			{
				blockMultiLevelElevatorGround.StartMotorSound(ebcd);
			}
			BlockMultiLevelElevatorGround.PlayDoorSound(groundBlockPos, true);
			closeDoors = false;
		}
	}
	
	public void OpenDoors()
	{
		doorJourneyLength = Vector3.Distance(leftStartPos, leftEndPos);
		float doorDistCovered = (Time.time - doorStartTime) * doorSpeed;
		float doorFracJourney = doorDistCovered / doorJourneyLength;
		if(leftDoorTransform.localPosition == leftEndPos)
		{
			BlockMultiLevelElevatorGround.PlayDoorSound(groundBlockPos, false);
			Vector3i newBlockPos = BlockMultiLevelElevatorGround.TargetVector3i(liftPlatform.position.y, groundBlockPos.x, groundBlockPos.z, true);
			BlockValue newBlockValue = worldBase.GetBlock(newBlockPos);
			Block newBlock = Block.list[newBlockValue.type];
			if(!String.IsNullOrEmpty(BlockMultiLevelElevatorGround.ShaftDoor1Transform(newBlock)) || !String.IsNullOrEmpty(BlockMultiLevelElevatorGround.ShaftDoor2Transform(newBlock)))
			{
				SetShaftDoorTransforms(newBlockPos, true);
			}
		}
		leftDoorTransform.localPosition = Vector3.Lerp(leftEndPos, leftStartPos, doorFracJourney);
		if(twoDoors)
		{
			rightDoorTransform.localPosition = Vector3.Lerp(rightEndPos, rightStartPos, doorFracJourney);
		}
		if(leftDoorTransform.localPosition == leftStartPos)
		{
			// BlockMultiLevelElevatorGround.DebugMsg("Doors opened");
			BlockMultiLevelElevatorGround.PlayDoorSound(groundBlockPos, true);
			BlockMultiLevelElevatorGround.PlayBell(groundBlockPos);
			openDoors = false;
		}
	}
	Transform vehicleParentTransform;
	public void MoveToFloor()
	{
		if(hasDoors && closeDoors)
		{
			return;
		}
		
		journeyLength = Vector3.Distance(endPos, startPos);
		float distCovered = (Time.time - startTime) * upDownSpeed;
		float fracJourney = distCovered / journeyLength;
		liftPlatform.position = Vector3.Lerp(startPos, endPos, fracJourney);
		if(vehicleEntity != null && containsVehicle)
		{
			vehicleUpDownSpeed = upDownSpeed+0.75f;
			vehicleJourneyLength = Vector3.Distance(vehicleEndPos, vehicleStartPos);
			float vehicleDistCovered = (Time.time - vehicleStartTime) * vehicleUpDownSpeed;;
			float vehicleFracJourney = vehicleDistCovered / vehicleJourneyLength;
			// vehicleEntity.transform.position = Vector3.Lerp(vehicleStartPos, vehicleEndPos, vehicleFracJourney);
		}
		if(TargetReached(endPos, liftPlatform.position))
		{
			if(motorSound != null)
			{
				if(motorSound.isPlaying)
				{
					motorSound.Stop();
				}
			}
			if(hasDoors)
			{
				doorStartTime = Time.time;
				openDoors = true;
			}
			Vector3i newBlockPos = BlockMultiLevelElevatorGround.TargetVector3i(liftPlatform.position.y, groundBlockPos.x, groundBlockPos.z, true);
			if(newBlockPos != null && newBlockPos != default(Vector3i))
			{
				BlockEntityData newEbcd = worldBase.ChunkClusters[0].GetBlockEntity(newBlockPos);
				if(newEbcd != null && newEbcd.bHasTransform)
				{
					BlockValue newBlockValue = worldBase.GetBlock(newBlockPos);
					Block newBlock = Block.list[newBlockValue.type];
				}
			}
			BlockMultiLevelElevatorGround.TriggerAllArrows(worldBase, groundBlockPos, true, true);
			moveElevator = false;
		}
	}
	BlockMultiLevelElevatorFloor targetFloorBlock;
	public void UpdatePostionData()
	{
		BlockMultiLevelElevatorGround.DebugMsg("UpdatePostionData");
		BlockMultiLevelElevatorGround.DebugMsg("startPos3i: " + startPos3i);
		BlockMultiLevelElevatorGround.DebugMsg("targetPos3i: " + targetPos3i);
		BlockValue startBlockValue = worldBase.GetBlock(startPos3i);
		Block startBlock = Block.list[startBlockValue.type];
		Type startType = Block.list[startBlockValue.type].GetType();
		BlockMultiLevelElevatorGround.DebugMsg("Start block name: " + startBlock.GetLocalizedBlockName());
		BlockMultiLevelElevatorGround.DebugMsg("start block Type: " + startType);
		if(startType == typeof(BlockMultiLevelElevatorGround) || startType == typeof(BlockMultiLevelElevatorFloor))
		{
			startBlockValue.meta = (byte) (startBlockValue.meta & ~(1 << 1));
			worldBase.SetBlockRPC(startPos3i, startBlockValue);
			BlockMultiLevelElevatorGround.DebugMsg("Start IsAtThisFloor: " + MultiLevelElevatorController.IsAtThisFloor(startBlockValue.meta));
		}
		
		BlockValue targetBlockValue = worldBase.GetBlock(targetPos3i);
		Block targetBlock = Block.list[targetBlockValue.type];
		Type targetType = Block.list[targetBlockValue.type].GetType();
		BlockMultiLevelElevatorGround.DebugMsg("Target block: " + targetBlock.GetLocalizedBlockName());
		BlockMultiLevelElevatorGround.DebugMsg("targetType: " + targetType);
		if(targetType == typeof(BlockMultiLevelElevatorGround) || targetType == typeof(BlockMultiLevelElevatorFloor))
		{
			targetBlockValue.meta = (byte) (targetBlockValue.meta | (1 << 1));
			worldBase.SetBlockRPC(targetPos3i, targetBlockValue);
			BlockMultiLevelElevatorGround.DebugMsg("Target IsAtThisFloor: " + MultiLevelElevatorController.IsAtThisFloor(targetBlockValue.meta));
		}
	}
	
	public bool TargetReached(Vector3 targetPos, Vector3 liftPos)
	{
		if(targetPos.y == liftPos.y)
		{
			UpdatePostionData();
			return true;
		}
		else return false;
	}
	
	private void ToolTipText(string str)
    {
		EntityPlayerLocal entity = GameManager.Instance.World.GetPrimaryPlayer();
		DateTime dteNextToolTipDisplayTime = default(DateTime);
        if (DateTime.Now > dteNextToolTipDisplayTime)
        {
            GameManager.ShowTooltip(entity, str);
			dteNextToolTipDisplayTime = DateTime.Now.AddSeconds(3);
        }
    }
	
	public bool playerOnLift;
	
	
	private Entity FindEntity(Transform t)
	{
		Entity componentInChildren = t.GetComponentInChildren<Entity>();
		if (componentInChildren)
		{
			return componentInChildren;
		}
		return t.GetComponentInParent<Entity>();
	}
	
	public bool containsVehicle;
	public Entity vehicleEntity;
	
	
	void OnTriggerEnter(Collider collider)
    {
		
			Entity entity = FindEntity(collider.gameObject.transform);
			if(entity != null)
			{
				// Debug.Log("got entity script: " + entity.ToString());
				Entity otherEntity = entity.AttachedToEntity;
				if(otherEntity != null)
				{
					// Debug.Log("got AttachedToEntity: " + otherEntity.ToString());
					EntityDriveable entityDriveable = otherEntity as EntityDriveable;
					if(entityDriveable != null)
					{
						// Debug.Log("got entityDriveable");
						vehicleEntity = otherEntity;
						Rigidbody rigidbody = otherEntity.transform.GetComponentInChildren<Rigidbody>();
						if(rigidbody != null)
						{
							// Debug.Log("got vehicles rigidbody");
							// Debug.Log("Kinematic: " + rigidbody.isKinematic);
							// Debug.Log("detectCollisions: " + rigidbody.detectCollisions);
							// rigidbody.isKinematic = false;
							// rigidbody.detectCollisions = true;
						}
						// vehicleParentTransform = vehicleEntity.transform;
						// vehicleEntity.transform.parent = liftPlatform;
						containsVehicle = true;
					}
				}
			}
    }
	
	void OnTriggerExit(Collider collider)
    {
		
		Entity entity = FindEntity(collider.gameObject.transform);
		if(entity != null)
		{
			// Debug.Log("got entity script: " + entity.ToString());
			Entity otherEntity = entity.AttachedToEntity;
			if(otherEntity != null)
			{
				// Debug.Log("got AttachedToEntity: " + otherEntity.ToString());
				EntityDriveable entityDriveable = otherEntity as EntityDriveable;
				if(entityDriveable != null)
				{
					// Debug.Log("got entityDriveable");
					vehicleEntity = otherEntity;
					if(vehicleEntity != null)
					{
						// vehicleEntity.transform.parent = vehicleParentTransform;
						containsVehicle = false;
					}
				}
			}
		}
    }
}
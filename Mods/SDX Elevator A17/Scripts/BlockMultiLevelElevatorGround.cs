using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;

// NEEDS DOING:
	// need to turn downSpeed back on now that there's a bool to tell what direction we are going
	// need to look at the lists we are converting to Vector3i arrays, i dont think the arrays are realy needed
	// need to prevent vehicles doing damage to the block
	


public class BlockMultiLevelElevatorHelper : MonoBehaviour
{
	public int selectedFloor;
}

public class BlockMultiLevelElevatorGround : BlockPowered
{
	public static Dictionary<string, Vector3i> groundFloors = new Dictionary<string, Vector3i>();
	public static Dictionary<string, List<Vector3i>> floors = new Dictionary<string, List<Vector3i>>();
	public static Dictionary<string, BlockMultiLevelElevatorHelper> helper = new Dictionary<string, BlockMultiLevelElevatorHelper>();
	public int selectedFloor;
	
	public MultiLevelElevatorController elevatorController;
	public Transform topFloorTransform;
	public float speedTest = 2.0f;
	
	public Vector3 leftShaftStartPos;
	public Vector3 leftShaftEndPos;
	public Vector3 rightShaftStartPos;
	public Vector3 rightShaftEndPos;
	
	// used for the placement rules
	public static void ToolTipTextOnClick(string str)
	{
		EntityPlayerLocal entity = GameManager.Instance.World.GetPrimaryPlayer();
		if (Input.GetMouseButtonDown(1))
			GameManager.ShowTooltip(entity, str);
	}
	
	public static Vector3 GetDoorOffSet(Block _block, int _doorNumber)
	{
		Vector3 doorOffSet = new Vector3(0,0,0);
		if (_block.Properties.Values.ContainsKey("Door" + _doorNumber + "OffSet"))
		{
			doorOffSet = StringParsers.ParseVector3(_block.Properties.Values["Door" + _doorNumber + "OffSet"], 0, -1);
		}
		return doorOffSet;
	}
	
	public static Vector3 GetShaftDoorOffSet(Block _block, int _doorNumber)
	{
		Vector3 shaftDoorOffSet = new Vector3(0,0,0);
		if (_block.Properties.Values.ContainsKey("ShaftDoor" + _doorNumber + "OffSet"))
		{
			shaftDoorOffSet = StringParsers.ParseVector3(_block.Properties.Values["ShaftDoor" + _doorNumber + "OffSet"], 0, -1);
		}
		return shaftDoorOffSet;
	}
	
	public static void TriggerAllArrows(WorldBase world, Vector3i _blockPos, bool goUp, bool turnOff)
	{
		string floorsKey = _blockPos.x + "_" + _blockPos.z + "_floorsList";
		if(BlockMultiLevelElevatorGround.floors.ContainsKey(floorsKey))
		{
			List<Vector3i> floorsList = new List<Vector3i>();
			if (BlockMultiLevelElevatorGround.floors.TryGetValue(floorsKey, out floorsList))
			{
				foreach(Vector3i floor in floorsList.ToList())
				{
					BlockEntityData _ebcd = world.ChunkClusters[0].GetBlockEntity(floor);
					if(_ebcd != null && _ebcd.bHasTransform)
					{
						BlockValue blockValue = world.GetBlock(floor);
						Block block = Block.list[blockValue.type];
						if(HasArrows(block))
						{
							Arrows(_ebcd, block, goUp, turnOff);
						}
					}
				}
			}
		}
	}
	
	public static void Arrows(BlockEntityData _ebcd, Block _block, bool goUp, bool turnOff)
	{
		List<Animator> upArrows = new List<Animator>();
		List<Animator> downArrows = new List<Animator>();
		List<Animator> arrows = new List<Animator>();
		
		Animator outsideUpArrow;
		Transform outsideUpArrowTransform = FindTransformNamed(_ebcd, UpArrowTransformOutside(_block));
		if(outsideUpArrowTransform != null && outsideUpArrowTransform != default(Transform))
		{
			outsideUpArrow = outsideUpArrowTransform.gameObject.GetComponent<Animator>();
			if(outsideUpArrow != null)
			{
				upArrows.Add(outsideUpArrow);
				arrows.Add(outsideUpArrow);
			}
		}
		
		Animator outsideDownArrow;
		Transform outsideDownArrowTransform = FindTransformNamed(_ebcd, DownArrowTransformOutside(_block));
		if(outsideDownArrowTransform != null && outsideDownArrowTransform != default(Transform))
		{
			outsideDownArrow = outsideDownArrowTransform.gameObject.GetComponent<Animator>();
			if(outsideDownArrow != null)
			{
				downArrows.Add(outsideDownArrow);
				arrows.Add(outsideDownArrow);
			}
		}
		
		Animator insideUpArrow;
		Transform insideUpArrowTransform = FindTransformNamed(_ebcd, UpArrowTransformInside(_block));
		if(insideUpArrowTransform != null && insideUpArrowTransform != default(Transform))
		{
			insideUpArrow = insideUpArrowTransform.gameObject.GetComponent<Animator>();
			if(insideUpArrow != null)
			{
				upArrows.Add(insideUpArrow);
				arrows.Add(insideUpArrow);
			}
		}
		
		Animator insideDownArrow;
		Transform insideDownArrowTransform = FindTransformNamed(_ebcd, DownArrowTransformInside(_block));
		if(insideDownArrowTransform != null && insideDownArrowTransform != default(Transform))
		{
			insideDownArrow = insideDownArrowTransform.gameObject.GetComponent<Animator>();
			if(insideDownArrow != null)
			{
				downArrows.Add(insideDownArrow);
				arrows.Add(insideDownArrow);
			}
		}
		if(turnOff)
		{
			if(arrows.Count != 0)
			{
				foreach(Animator arrowAnimator in arrows.ToList())
				{
					arrowAnimator.SetBool("arrowOn", false);
				}
			}
			return;
		}
		if(goUp)
		{
			if(upArrows.Count != 0)
			{
				foreach(Animator upArrowAnimator in upArrows.ToList())
				{
					upArrowAnimator.SetBool("arrowOn", true);
				}
			}
			if(downArrows.Count != 0)
			{
				foreach(Animator downArrowAnimator in downArrows.ToList())
				{
					downArrowAnimator.SetBool("arrowOn", false);
				}
			}
			return;
		}
		else
		{
			if(downArrows.Count != 0)
			{
				foreach(Animator downArrowAnimator in downArrows.ToList())
				{
					downArrowAnimator.SetBool("arrowOn", true);
				}
			}
			if(upArrows.Count != 0)
			{
				foreach(Animator upArrowAnimator in upArrows.ToList())
				{
					upArrowAnimator.SetBool("arrowOn", false);
				}
			}
			return;
		}
	}
	
	public static int GetSetSelectedFloor(Vector3i _blockPos, int floorToSet, bool getValue)
	{
		int newSelectedFloor = 0;
		WorldBase world = GameManager.Instance.World;
		string key = _blockPos.x + "_" + _blockPos.z + "_helperScript";
		if(!helper.ContainsKey(key))
		{
			BlockEntityData _ebcd = world.ChunkClusters[0].GetBlockEntity(_blockPos);
			if(_ebcd != null && _ebcd.bHasTransform)
			{
				if(!_ebcd.transform.gameObject.GetComponent<BlockMultiLevelElevatorHelper>())
				{
					_ebcd.transform.gameObject.AddComponent<BlockMultiLevelElevatorHelper>();
				}
				helper.Add(key, _ebcd.transform.gameObject.GetComponent<BlockMultiLevelElevatorHelper>());
			}
			else
				helper.Add(key, new BlockMultiLevelElevatorHelper());
		}
		
		if(helper.ContainsKey(key))
		{
			BlockMultiLevelElevatorHelper helperScript = default(BlockMultiLevelElevatorHelper);
			if (helper.TryGetValue(key, out helperScript))
			{
				if(!getValue)
				{
					helperScript.selectedFloor = floorToSet;
					return floorToSet;
				}
				return helperScript.selectedFloor;
			}
		}
		return newSelectedFloor;
	}
	
	public static float ElevatorDownSpeed(Block _block)
	{
		float downSpeed = 2.5f;
		if (_block.Properties.Values.ContainsKey("ElevatorDownSpeed"))
		{
			float.TryParse(_block.Properties.Values["ElevatorDownSpeed"], out downSpeed);
		}
		return downSpeed;
	}
	
	public static float ElevatorUpSpeed(Block _block)
	{
		float upSpeed = 2.5f;
		if (_block.Properties.Values.ContainsKey("ElevatorUpSpeed"))
		{
			float.TryParse(_block.Properties.Values["ElevatorUpSpeed"], out upSpeed);
		}
		return upSpeed;
	}
	
	public static float DoorSpeed(Block _block)
	{
		float doorSpeed = 0.75f;
		if (_block.Properties.Values.ContainsKey("DoorSpeed"))
		{
			float.TryParse(_block.Properties.Values["DoorSpeed"], out doorSpeed);
		}
		return doorSpeed;
	}
	
	public static bool ShowRaycastHitName(Block _block)
	{
		bool showRaycastHitName = false;
		if (_block.Properties.Values.ContainsKey("ShowRaycastHitName"))
		{
			bool.TryParse(_block.Properties.Values["ShowRaycastHitName"], out showRaycastHitName);
		}
		return showRaycastHitName;
	}
	
	public static bool RequiresPower(Block _block)
	{
		bool requiresPower = false;
		if (_block.Properties.Values.ContainsKey("RequiresPower"))
		{
			bool.TryParse(_block.Properties.Values["RequiresPower"], out requiresPower);
		}
		return requiresPower;
	}
	
	public static bool DebugSpeed(Block _block)
	{
		bool debugSpeed = false;
		if (_block.Properties.Values.ContainsKey("DebugSpeed"))
		{
			bool.TryParse(_block.Properties.Values["DebugSpeed"], out debugSpeed);
		}
		return debugSpeed;
	}
	
	public static string InsideButtonTransform(Block _block)
	{
		string insideButtonTransform = "";
		if (_block.Properties.Values.ContainsKey("InsideButtonTransform"))
		{
			insideButtonTransform = _block.Properties.Values["InsideButtonTransform"];
		}
		return insideButtonTransform;
	}
	
	public static string CallButtonTransform(Block _block)
	{
		string callButtonNameString = "";
		if (_block.Properties.Values.ContainsKey("CallButtonTransform"))
		{
			callButtonNameString = _block.Properties.Values["CallButtonTransform"];
		}
		return callButtonNameString;
	}
	
	public static bool HasArrows(Block _block)
	{
		bool hasArrows = false;
		if(!String.IsNullOrEmpty(UpArrowTransformInside(_block)) && !String.IsNullOrEmpty(DownArrowTransformInside(_block)) || !String.IsNullOrEmpty(UpArrowTransformOutside(_block)) && !String.IsNullOrEmpty(DownArrowTransformOutside(_block)))
		{
			hasArrows = true;
		}
		return hasArrows;
	}
	
	public static string UpArrowTransformInside(Block _block)
	{
		string upArrowTransformInside = "";
		if (_block.Properties.Values.ContainsKey("UpArrowTransformInside"))
		{
			upArrowTransformInside = _block.Properties.Values["UpArrowTransformInside"];
		}
		return upArrowTransformInside;
	}
	
	public static string DownArrowTransformInside(Block _block)
	{
		string downArrowTransformInside = "";
		if (_block.Properties.Values.ContainsKey("DownArrowTransformInside"))
		{
			downArrowTransformInside = _block.Properties.Values["DownArrowTransformInside"];
		}
		return downArrowTransformInside;
	}
	
	public static string UpArrowTransformOutside(Block _block)
	{
		string upArrowTransformOutside = "";
		if (_block.Properties.Values.ContainsKey("UpArrowTransformOutside"))
		{
			upArrowTransformOutside = _block.Properties.Values["UpArrowTransformOutside"];
		}
		return upArrowTransformOutside;
	}
	
	public static string DownArrowTransformOutside(Block _block)
	{
		string downArrowTransformOutside = "";
		if (_block.Properties.Values.ContainsKey("DownArrowTransformOutside"))
		{
			downArrowTransformOutside = _block.Properties.Values["DownArrowTransformOutside"];
		}
		return downArrowTransformOutside;
	}
	
	public string GroundBlockName()
	{
		string groundNameString = "Ground Floor";
		if (this.Properties.Values.ContainsKey("GroundName"))
		{
			groundNameString = this.Properties.Values["GroundName"];
		}
		return groundNameString;
	}
	
	public static string ButtonClickSoundTransform(Block _block)
	{
		string buttonClickSoundTransform = "";
		if (_block.Properties.Values.ContainsKey("ButtonClickSoundTransform"))
		{
			buttonClickSoundTransform = _block.Properties.Values["ButtonClickSoundTransform"];
		}
		return buttonClickSoundTransform;
	}
	
	public static string BellSoundTransform(Block _block)
	{
		string bellSoundTransform = "";
		if (_block.Properties.Values.ContainsKey("BellSoundTransform"))
		{
			bellSoundTransform = _block.Properties.Values["BellSoundTransform"];
		}
		return bellSoundTransform;
	}
	
	public static string MotorSoundTransform(Block _block)
	{
		string motorSoundTransform = "";
		if (_block.Properties.Values.ContainsKey("MotorSoundTransform"))
		{
			motorSoundTransform = _block.Properties.Values["MotorSoundTransform"];
		}
		return motorSoundTransform;
	}
	
	public static string DoorSoundTransform(Block _block)
	{
		string doorSoundTransform = "";
		if (_block.Properties.Values.ContainsKey("DoorSoundTransform"))
		{
			doorSoundTransform = _block.Properties.Values["DoorSoundTransform"];
		}
		return doorSoundTransform;
	}
	
	public static string Door1Transform(Block _block)
	{
		string door1Transform = "";
		if (_block.Properties.Values.ContainsKey("Door1Transform"))
		{
			door1Transform = _block.Properties.Values["Door1Transform"];
		}
		return door1Transform;
	}
	
	public static string Door2Transform(Block _block)
	{
		string door2Transform = "";
		if (_block.Properties.Values.ContainsKey("Door2Transform"))
		{
			door2Transform = _block.Properties.Values["Door2Transform"];
		}
		return door2Transform;
	}
	
	public static string ShaftDoor1Transform(Block _block)
	{
		string shaftDoor1Transform = "";
		if (_block.Properties.Values.ContainsKey("ShaftDoor1Transform"))
		{
			shaftDoor1Transform = _block.Properties.Values["ShaftDoor1Transform"];
		}
		return shaftDoor1Transform;
	}
	
	public static string ShaftDoor2Transform(Block _block)
	{
		string shaftDoor2Transform = "";
		if (_block.Properties.Values.ContainsKey("ShaftDoor2Transform"))
		{
			shaftDoor2Transform = _block.Properties.Values["ShaftDoor2Transform"];
		}
		return shaftDoor2Transform;
	}
	
	public static string TransformToMove(Block _block)
	{
		string transformToMove = "";
		if (_block.Properties.Values.ContainsKey("TransformToMove"))
		{
			transformToMove = _block.Properties.Values["TransformToMove"];
		}
		return transformToMove;
	}
	
	public static string PlatformBaseTransform(Block _block)
	{
		string platformBaseTransform = "";
		if (_block.Properties.Values.ContainsKey("PlatformBaseTransform"))
		{
			platformBaseTransform = _block.Properties.Values["PlatformBaseTransform"];
		}
		return platformBaseTransform;
	}
	
	public static void PlayDoorSound(Vector3i _blockPos, bool stop)
	{
		WorldBase world = GameManager.Instance.World;
		BlockEntityData _ebcd = world.ChunkClusters[0].GetBlockEntity(_blockPos);
		if(_ebcd != null && _ebcd.bHasTransform)
		{
			BlockValue blockValue = world.GetBlock(_blockPos);
			Block block = Block.list[blockValue.type];
			Transform doorSoundTransform = FindTransformNamed(_ebcd, DoorSoundTransform(block));
			if(doorSoundTransform != null && doorSoundTransform != default(Transform))
			{
				AudioSource doorSound = doorSoundTransform.gameObject.GetComponent<AudioSource>();
				if(doorSound != null)
				{
					if(!stop)
					{
						doorSound.Play();
					}
					else
					{
						if(doorSound.isPlaying)
						{
							doorSound.Stop();
						}
					}
				}
			}
		}
		return;
	}
	
	public static void PlayBell(Vector3i _blockPos)
	{
		WorldBase world = GameManager.Instance.World;
		BlockEntityData _ebcd = world.ChunkClusters[0].GetBlockEntity(_blockPos);
		if(_ebcd != null && _ebcd.bHasTransform)
		{
			BlockValue blockValue = world.GetBlock(_blockPos);
			Block block = Block.list[blockValue.type];
			Transform bellSoundTransform = FindTransformNamed(_ebcd, BellSoundTransform(block));
			if(bellSoundTransform != null && bellSoundTransform != default(Transform))
			{
				AudioSource bellSound = bellSoundTransform.gameObject.GetComponent<AudioSource>();
				if(bellSound != null)
				{
					bellSound.Play();
				}
			}
		}
		return;
	}
	
	public static void ButtonClick(Vector3i _blockPos)
	{
		WorldBase world = GameManager.Instance.World;
		BlockEntityData _ebcd = world.ChunkClusters[0].GetBlockEntity(_blockPos);
		if(_ebcd != null && _ebcd.bHasTransform)
		{
			BlockValue blockValue = world.GetBlock(_blockPos);
			Block block = Block.list[blockValue.type];
			Transform clickSoundTransform = FindTransformNamed(_ebcd, ButtonClickSoundTransform(block));
			if(clickSoundTransform != null && clickSoundTransform != default(Transform))
			{
				AudioSource clickSound = clickSoundTransform.gameObject.GetComponent<AudioSource>();
				if(clickSound != null)
				{
					clickSound.Play();
				}
			}
		}
		return;
	}
	
	public static string LookRay(Block _block, BlockValue _blockValue, EntityAlive _entity)
	{
		Transform entityTransform = _entity.transform;
		if(entityTransform == null)
			DebugMsg("entityTransform is null");
		GameObject entityGameObject = entityTransform.gameObject;
		if(entityGameObject == null)
			DebugMsg("entityGameObject null");
		Collider localPlayerCollider = entityGameObject.GetComponent<Collider>();
		Collider otherCollider = localPlayerCollider;
		GameObject playerGameObject = localPlayerCollider.gameObject;
		Camera _playerCam = localPlayerCollider.transform.GetComponentInChildren<Camera>();
		if(_playerCam == null || otherCollider == null)
		{
			return "Use Lift";
		}
		Transform camTransform = entityTransform;
		Transform[] playerTransforms = localPlayerCollider.transform.GetComponentsInChildren<Transform>();
		foreach (Transform newTransform in playerTransforms)
		{
			if(newTransform.GetComponent<Camera>())
			{
				camTransform = newTransform;
			}
		}
		int layer = _entity.GetModelLayer();
		int layerMask = 1 << 16;
        RaycastHit hit;
        if (Physics.Raycast(camTransform.position, camTransform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity, layerMask))
		{
			if(ShowRaycastHitName(_block))
				Debug.Log("Physics.Raycast, hit.transform.name: " + hit.transform.name);
			DebugMsg("hit layer: " + hit.transform.gameObject.layer);
			Type blockType = Block.list[_blockValue.type].GetType();
			if(blockType == typeof(BlockMultiLevelElevatorFloor))
			{
				if(String.IsNullOrEmpty(CallButtonTransform(_block)))
				{
					return "Call Elevator";
				}
				else
				{
					if(hit.transform.name == CallButtonTransform(_block))
					{
						return "Call Elevator";
					}
					else
						return "No Hit";
				}
			}
			if(blockType == typeof(BlockMultiLevelElevatorGround))
			{
				
				if(String.IsNullOrEmpty(CallButtonTransform(_block)) || String.IsNullOrEmpty(InsideButtonTransform(_block)))
				{
					Debug.Log("Error: Both buttons must be defined in xml for the control floor");
					return "XML Error";
				}
				else
				{
					if(hit.transform.name == InsideButtonTransform(_block))
					{
						return "Use";
					}
					if(hit.transform.name == CallButtonTransform(_block))
					{
						return "Call Elevator";
					}
					return "No Hit";
				}
			}
			
		}
		return "No Hit";
	}
	
	public static bool GoingUp(float _startPosY, float _endPosY)
	{
		if(_startPosY <= _endPosY)
		{
			return true;
		}
		return false;
	}
	
	public override bool OnBlockActivated(int _indexInBlockActivationCommands, WorldBase _world, int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
	{
		if(LookRay(this, _blockValue, _player) == "Call Elevator")
			ButtonClick(_blockPos);
		BlockEntityData _ebcd = _world.ChunkClusters[_cIdx].GetBlockEntity(_blockPos);
		if(_ebcd != null && _ebcd.bHasTransform)
		{
			Transform elevatorTransform = FindTransformNamed(_ebcd, PlatformBaseTransform(this));
			if(elevatorTransform != null && elevatorTransform != default(Transform))
			{
				if(this.elevatorController == null && !elevatorTransform.gameObject.GetComponent<MultiLevelElevatorController>())
				{
					elevatorTransform.gameObject.AddComponent<MultiLevelElevatorController>();
				}
			}
			this.elevatorController = elevatorTransform.gameObject.GetComponent<MultiLevelElevatorController>();
		}
		else
		{
			return false;
		}
		if(this.elevatorController == null)
		{
			DebugMsg("OnBlockActivated: elevatorController NULL");
			return true;
		}
		else
			DebugMsg("OnBlockActivated: elevatorController FOUND");
			DebugMsg("OnBlockActivated: Ground transform: " + _ebcd.transform.localPosition);
			if(this.elevatorController.moveElevator)
			{
				ToolTipText("Elevator in use");
				return true;
			}
			Transform liftPlatform = FindTransformNamed(_ebcd, TransformToMove(this)); // This is the transform we move
			if(liftPlatform != null && liftPlatform != default(Transform) && this.elevatorController != null)
			{
				DebugMsg("OnBlockActivated: liftPlatform found");
				this.elevatorController.liftPlatform = liftPlatform;
				this.elevatorController.blockMultiLevelElevatorGround = this;
				this.elevatorController.groundBlockPos = _blockPos;
				this.elevatorController.groundBlockValue = _blockValue;
				this.elevatorController.ebcd = _ebcd;
				bool needToMove = false;
				int floorNumber = GetSetSelectedFloor(_blockPos, 0, true);
				bool myProtectedLand = _world.IsMyLandProtectedBlock(_blockPos, _world.GetGameManager().GetPersistentLocalPlayer(), false);
					if(RequiresPower(this) && !isBlockPoweredUp(_blockPos, _cIdx))
					{
						if(floorNumber != 999)
						{
							ToolTipText(GroundBlockName() + " Needs power");
							return false;
						}
						else
						{
							if(!myProtectedLand)
							{
								ToolTipText(GroundBlockName() + " Needs power");
								return false;
							}
							else
								ToolTipText("Using BackUp Power");
						}
					}
					if(LookRay(this, _blockValue, _player) == "Use")
					{
						
						if(floorNumber == 999)
						{
							if(this.elevatorController.liftPlatform.position.y != _ebcd.transform.position.y)
							{
								needToMove = true;
								this.elevatorController.startPos = liftPlatform.position;
								this.elevatorController.endPos = _ebcd.transform.position;
								this.elevatorController.upDownSpeed = ElevatorUpSpeed(this);
								// shaftDoorVector = _blockPos;
							}
						}
						else
						{
							needToMove = true;
							this.elevatorController.startPos = _ebcd.transform.position;
							Vector3 targetPos = default(Vector3);
							Vector3i targetPos3i = TargetVector3i(floorNumber, _blockPos.x, _blockPos.z, false);
							if(targetPos3i != default(Vector3i))
							{
								BlockEntityData targetEbcd = _world.ChunkClusters[_cIdx].GetBlockEntity(targetPos3i);
								if(targetEbcd != null && targetEbcd.bHasTransform)
								{
									targetPos = targetEbcd.transform.position;
									this.elevatorController.targetPos3i = targetPos3i;
								}
							}
							if(targetPos == default(Vector3))
							{
								needToMove = false;
								DebugMsg("TargetVector3 for selectedFloor returned default");
							}
							this.elevatorController.startPos = liftPlatform.position;
							this.elevatorController.endPos = new Vector3(this.elevatorController.liftPlatform.position.x, targetPos.y, this.elevatorController.liftPlatform.position.z);
							if(DebugSpeed(this))
							{
								this.elevatorController.upDownSpeed = speedTest;
							}
							else
								this.elevatorController.upDownSpeed = ElevatorUpSpeed(this);
							// Debug.Log("speed at start: " + this.elevatorController.upDownSpeed);
						}
						GetSetSelectedFloor(_blockPos, 0, false);
						this.num1 = "";
						this.num2 = "";
						this.num3 = "";
						this.nums = "";
					}
				
				if(LookRay(this, _blockValue, _player) == "Call Elevator")
				{
					if(this.elevatorController.liftPlatform.position.y != _ebcd.transform.position.y)
					{
						DebugMsg("Ground button _blockPos: " + _blockPos);
						needToMove = true;
						this.elevatorController.startPos = this.elevatorController.liftPlatform.position;
						this.elevatorController.endPos = _ebcd.transform.position;
						this.elevatorController.upDownSpeed = ElevatorUpSpeed(this);
					}
					else
						ToolTipText("Elevator is already at this floor");
				}
				if(this.elevatorController.startPos.y == this.elevatorController.endPos.y)
				{
					if(LookRay(this, _blockValue, _player) == "Use" || LookRay(this, _blockValue, _player) == "Call Elevator")
					{
						ToolTipText("Elevator is already at that floor");
					}
					needToMove = false;
				}
				if(this.elevatorController.liftPlatform.position.y == _ebcd.transform.position.y)
				{
					this.elevatorController.startPos3i = _blockPos;
				}
				else
				{
					if(!this.elevatorController.moveElevator)
					{
						// check each vector3i's y against liftPlatform.position.y until we find the floor its on, then set startPos3i to that floor
						this.elevatorController.startPos3i = TargetVector3i(this.elevatorController.liftPlatform.position.y, _blockPos.x, _blockPos.z, true);
					}
					else
						DebugMsg("startPos3i was left untouched");
					
				}
				if(needToMove)
				{
					
					bool goingUp = GoingUp(this.elevatorController.startPos.y, this.elevatorController.endPos.y);
					if(goingUp)
					{
						ToolTipText("Going Up");
						
					}
					else
						ToolTipText("Going Down");
					
					if(!String.IsNullOrEmpty(Door1Transform(this)))
					{
						Transform leftDoorTransform = FindTransformNamed(_ebcd, Door1Transform(this));
						if(leftDoorTransform != null && leftDoorTransform != default(Transform))
						{
							this.elevatorController.hasDoors = !String.IsNullOrEmpty(Door1Transform(this));
							this.elevatorController.twoDoors = !String.IsNullOrEmpty(Door2Transform(this));
							this.elevatorController.doorSpeed = DoorSpeed(this);
							this.elevatorController.leftDoorTransform = leftDoorTransform;
							if(!String.IsNullOrEmpty(Door2Transform(this)))
							{
								Transform rightDoorTransform = FindTransformNamed(_ebcd, Door2Transform(this));
								if(rightDoorTransform != null && rightDoorTransform != default(Transform))
								{
									this.elevatorController.rightDoorTransform = rightDoorTransform;
								}
							}
							this.elevatorController.doorStartTime = Time.time;
							this.elevatorController.closeDoors = true;
						}
					}
					if(!this.elevatorController.closeDoors)
					{
						this.elevatorController.startTime = Time.time;
						this.elevatorController.moveElevator = true;
						StartMotorSound(_ebcd);
						TriggerAllArrows(_world, _blockPos, goingUp, false);
					}
					if(!String.IsNullOrEmpty(BlockMultiLevelElevatorGround.ShaftDoor1Transform(this)) || !String.IsNullOrEmpty(BlockMultiLevelElevatorGround.ShaftDoor2Transform(this)))
					{
						this.elevatorController.twoShaftDoors = !String.IsNullOrEmpty(ShaftDoor2Transform(this));
						this.elevatorController.SetShaftDoorTransforms(this.elevatorController.startPos3i, false);
					}
					
				}
			}
		return true;
	}
	
	public void StartMotorSound(BlockEntityData _ebcd)
	{
		Transform motorTransform = FindTransformNamed(_ebcd, MotorSoundTransform(this));
		if(motorTransform == null || motorTransform == default(Transform))
		{
			return;
		}
		this.elevatorController.motorSound = motorTransform.gameObject.GetComponent<AudioSource>();
		if(this.elevatorController.motorSound != null)
		{
			if(!this.elevatorController.motorSound.isPlaying)
			{
				this.elevatorController.motorSound.Play();
				return;
			}
		}
	}
	
	public static Transform FindTransformNamed(BlockEntityData _ebcd, string _name)
	{
		Transform[] transforms = _ebcd.transform.GetComponentsInChildren<Transform>();
		foreach (Transform newTransform in transforms)
		{
			if(newTransform.gameObject.name == _name)
			{
				DebugMsg("FindTransformNamed match found: " + newTransform.gameObject.name);
				return newTransform;
			}
		}
		return default(Transform);
	}
	
	public static void DestroyHelperScript(Vector3i _blockPos)
	{
		string key = _blockPos.x + "_" + _blockPos.z + "_helperScript";
		if(helper.ContainsKey(key))
		{
			BlockMultiLevelElevatorHelper helperScript = default(BlockMultiLevelElevatorHelper);
			if (helper.TryGetValue(key, out helperScript))
			{
				if(helperScript != null)
				{
					MonoBehaviour.Destroy(helperScript);
				}
			}
		}
	}
	
	public override void OnBlockUnloaded(WorldBase _world, int _clrIdx, Vector3i _blockPos, BlockValue _blockValue)
	{
		if (!_blockValue.ischild)
		{
			if(this.elevatorController != null)
			{
				this.elevatorController.enabled = false;
				MonoBehaviour.Destroy(this.elevatorController);
			}
			DestroyHelperScript(_blockPos);
		}
		base.OnBlockUnloaded(_world, _clrIdx, _blockPos, _blockValue);
	}
	
	public override void OnBlockEntityTransformAfterActivated(WorldBase _world, Vector3i _blockPos, int _cIdx, BlockValue _blockValue, BlockEntityData _ebcd)
	{
		base.OnBlockEntityTransformAfterActivated(_world, _blockPos, _cIdx, _blockValue, _ebcd);
		if(!String.IsNullOrEmpty(ShaftDoor1Transform(this)))
		{
			Transform leftShaftDoorTransform = FindTransformNamed(_ebcd, ShaftDoor1Transform(this));
			if(leftShaftDoorTransform != null && leftShaftDoorTransform != default(Transform))
			{
				this.leftShaftStartPos = leftShaftDoorTransform.localPosition;
				this.leftShaftEndPos = leftShaftDoorTransform.localPosition + GetShaftDoorOffSet(this, 1);
				if(!MultiLevelElevatorController.IsAtThisFloor(_blockValue.meta))
				{
					leftShaftDoorTransform.localPosition = leftShaftDoorTransform.localPosition + BlockMultiLevelElevatorGround.GetShaftDoorOffSet(this, 1);
				}
			}	
		}
		if(!String.IsNullOrEmpty(ShaftDoor2Transform(this)))
		{
			Transform rightShaftDoorTransform = FindTransformNamed(_ebcd, ShaftDoor2Transform(this));
			if(rightShaftDoorTransform != null && rightShaftDoorTransform != default(Transform))
			{
				this.rightShaftStartPos = rightShaftDoorTransform.localPosition;
				this.rightShaftEndPos = rightShaftDoorTransform.localPosition + GetShaftDoorOffSet(this, 2);
				if(!MultiLevelElevatorController.IsAtThisFloor(_blockValue.meta))
				{
					rightShaftDoorTransform.localPosition = rightShaftDoorTransform.localPosition + BlockMultiLevelElevatorGround.GetShaftDoorOffSet(this, 2);
				}
			}
		}
		string floorsKey = _blockPos.x + "_" + _blockPos.z + "_floorsList";
		if(!BlockMultiLevelElevatorGround.floors.ContainsKey(floorsKey))
		{
			BlockMultiLevelElevatorGround.floors.Add(floorsKey, new List<Vector3i>());
		}
		if(BlockMultiLevelElevatorGround.floors.ContainsKey(floorsKey))
		{
			List<Vector3i> list = new List<Vector3i>();
			if (BlockMultiLevelElevatorGround.floors.TryGetValue(floorsKey, out list))
			{
				bool addFloor = true;
				Vector3i[] floorVectorArray = list.ToArray();
				foreach(Vector3i arrayFloor in floorVectorArray)
				{
					DebugMsg("new foreach");
					if(arrayFloor.y == _blockPos.y)
					{
						addFloor = false;
						break;
					}
				}
				if(addFloor)
				{
					list.Add(_blockPos);
				}
			}
		}
		if(_ebcd != null && _ebcd.bHasTransform)
		{
			Transform elevatorTransform = FindTransformNamed(_ebcd, PlatformBaseTransform(this));
			if(elevatorTransform != null && elevatorTransform != default(Transform))
			{
				if(this.elevatorController == null && !elevatorTransform.gameObject.GetComponent<MultiLevelElevatorController>())
				{
					elevatorTransform.gameObject.AddComponent<MultiLevelElevatorController>();
					CheckForceBellow(_blockPos);
				}
			}
			this.elevatorController = elevatorTransform.gameObject.GetComponent<MultiLevelElevatorController>();
		}
		else
		{
			this.OnBlockEntityTransformAfterActivated(_world, _blockPos, _cIdx, _blockValue, _ebcd);
			return;
		}
		if(this.elevatorController == null)
		{
			this.OnBlockEntityTransformAfterActivated(_world, _blockPos, _cIdx, _blockValue, _ebcd);
			return;
		}
		this.elevatorController.blockMultiLevelElevatorGround = this;
		this.elevatorController.groundBlockPos = _blockPos;
		this.elevatorController.groundBlockValue = _blockValue;
		if(!String.IsNullOrEmpty(Door1Transform(this)))
		{
			Transform leftDoorTransform = FindTransformNamed(_ebcd, Door1Transform(this));
			if(leftDoorTransform != null && leftDoorTransform != default(Transform))
			{
				this.elevatorController.leftDoorTransform = leftDoorTransform;
				this.elevatorController.leftStartPos = new Vector3(leftDoorTransform.localPosition.x, leftDoorTransform.localPosition.y, leftDoorTransform.localPosition.z);
				this.elevatorController.leftEndPos = leftDoorTransform.localPosition + GetDoorOffSet(this, 1);
			}
			if(!String.IsNullOrEmpty(Door2Transform(this)))
			{
				Transform rightDoorTransform = FindTransformNamed(_ebcd, Door2Transform(this));
				if(rightDoorTransform != null && rightDoorTransform != default(Transform))
				{
					this.elevatorController.rightStartPos = new Vector3(rightDoorTransform.localPosition.x, rightDoorTransform.localPosition.y, rightDoorTransform.localPosition.z);
					this.elevatorController.rightEndPos = rightDoorTransform.localPosition + GetDoorOffSet(this, 2);
					this.elevatorController.rightDoorTransform = rightDoorTransform;
				}
			}	
		}
		string key = _blockPos.x + "_" + _blockPos.z + "_Ground";
		if (!groundFloors.ContainsKey(key))
		{
			try
			{
				groundFloors.Add(key, _blockPos);
			}
			catch (ArgumentException)
			{
				DebugMsg("An element with Key " + key + " already exists.");
			}			
			Vector3i checkedVector3i = default(Vector3i);
			if (groundFloors.TryGetValue(key, out checkedVector3i))
			{
				DebugMsg("Ground Floor For: " + key + " Saved Floor Vector3i: " + checkedVector3i);
				DebugMsg("Ground pos: " + _blockPos);
			}
		}
		else
		{
			DebugMsg("Key already in Dictionary");
			Vector3i vector3i = default(Vector3i);
			if (groundFloors.TryGetValue(key, out vector3i))
			{
				DebugMsg("Key already in Dictionary For: " + key + "Saved Floor Vector3i: " + vector3i);
			}
		}
	}
	
	public static Vector3 ShaftDoorEndPos(Vector3 _startPos, Block _block, int doorNum)
	{
		return _startPos + GetShaftDoorOffSet(_block, doorNum);
	}
	
	public override string GetActivationText(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing)
	{
		string text = "";
		if(LookRay(this, _blockValue, _entityFocusing) == "XML Error")
		{
			return "Error: Missing button transform in xml";
		}
		if(LookRay(this, _blockValue, _entityFocusing) == "Use")
		{
			if(DebugSpeed(this) && this.elevatorController != null)
			{
				if (Input.GetKeyUp(KeyCode.KeypadMinus))
				{
					this.speedTest -= 0.1f;
					Debug.Log("speed adjusted: " + this.speedTest);
				}
				if (Input.GetKeyUp(KeyCode.KeypadPlus))
				{
					this.speedTest += 0.1f;
					Debug.Log("speed adjusted: " + this.speedTest);
				}
				if (Input.GetKeyUp(KeyCode.KeypadEnter))
				{
					Debug.Log("speedTest: " + this.speedTest);
				}
			}
			GetKeyPress(this, _blockPos);
			int floorNumber = GetSetSelectedFloor(_blockPos, 0, true);
			if(floorNumber == 0)
			{
				text = "Select a Floor";
			}
			else
			{
				if(floorNumber <= FloorCount(_blockPos))
				{
					text = "Goto Floor " + floorNumber;
				}
				else
				{
					if(floorNumber == 999)
					{
						text = "Goto " + GroundBlockName();
					}
					else
						text = "Floor " + floorNumber + " not found";
				}
			}
		}
		if(LookRay(this, _blockValue, _entityFocusing) == "Call Elevator")
		{
			if(this.elevatorController != null)
			{
				if(this.elevatorController.liftPlatform != null && this.elevatorController.liftPlatform != default(Transform))
				{
					BlockEntityData _ebcd = _world.ChunkClusters[_clrIdx].GetBlockEntity(_blockPos);
					if(_ebcd != null && _ebcd.bHasTransform)
					{
						if(this.elevatorController.liftPlatform.position.y == _ebcd.transform.position.y)
						{
							return "Floor " + BlockMultiLevelElevatorGround.GetFloorNumber(_blockPos);
						}
					}
					
				}
			}
			text = LookRay(this, _blockValue, _entityFocusing) + " (" + GetFloorNumber(_blockPos) + ")";
		}
		return text;
	}
	
	public static int FloorCount(Vector3i _blockPos)
	{
		string floorsKey = _blockPos.x + "_" + _blockPos.z + "_floorsList";
		if(BlockMultiLevelElevatorGround.floors.ContainsKey(floorsKey))
		{
			List<Vector3i> list = new List<Vector3i>();
			if (BlockMultiLevelElevatorGround.floors.TryGetValue(floorsKey, out list))
			{
				return list.Count;
			}
		}
		return 0;
	}
	
	private BlockActivationCommand[] SZ = new BlockActivationCommand[]
	{
		new BlockActivationCommand("take", "hand", true)
	};
	
	public override BlockActivationCommand[] GetBlockActivationCommands(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing)
	{
		return this.SZ;
	}

	public override void OnBlockAdded(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
	{
		base.OnBlockAdded(world, _chunk, _blockPos, _blockValue);
		if (!_blockValue.ischild)
		{
			_blockValue.meta = (byte) (_blockValue.meta | (1 << 1));
			world.SetBlockRPC(_blockPos, _blockValue);
		}
	}
	
	public static bool isBlockPoweredUp(Vector3i _blockPos, int _clrIdx)
	{
		WorldBase world = GameManager.Instance.World;
		TileEntityPowered tileEntityPowered = (TileEntityPowered)GameManager.Instance.World.GetTileEntity(_clrIdx, _blockPos);
		if (tileEntityPowered != null)
		{
			if(tileEntityPowered.IsPowered)
			{
				DebugMsg("Block Power Is On");
				return true;
			}
		}
		DebugMsg("Block Power Is Off");
		return false;
	}
	
	void CheckForceBellow(Vector3i _blockPos)
	{
		DebugMsg("CheckForceBellow");
		WorldBase _world = GameManager.Instance.World;
		BlockValue _blockValue = _world.GetBlock(_blockPos);
		if(!MultiLevelElevatorController.IsAtThisFloor(_blockValue.meta))
		{
			DebugMsg("IsAtThisFloor false (ground)");
			BlockEntityData _ebcd = _world.ChunkClusters[0].GetBlockEntity(_blockPos);
			if(_ebcd != null && _ebcd.bHasTransform)
			{
				DebugMsg("got ebcd (ground)");
				Transform liftPlatform = FindTransformNamed(_ebcd, TransformToMove(this));
				if(liftPlatform != null && liftPlatform != default(Transform))
				{
					DebugMsg("got liftPlatform (ground)");
					if(liftPlatform.position.y == _ebcd.transform.position.y)
					{
						string key = _blockPos.x + "_" + _blockPos.z + "_floorsList";
						if(floors.ContainsKey(key))
						{
							DebugMsg("floors.ContainsKey true");
							List<Vector3i> list = new List<Vector3i>();
							if (floors.TryGetValue(key, out list))
							{
								DebugMsg("got list, creating array");
								Vector3i[] floorVectorArray = list.ToArray();
								foreach(Vector3i arrayFloor in floorVectorArray)
								{
									DebugMsg("new foreach");
									if(arrayFloor.y <= _blockPos.y)
									{
										DebugMsg("floor is lower than ground (array)");
										BlockValue arrayBlockValue = _world.GetBlock(arrayFloor);
										if(MultiLevelElevatorController.IsAtThisFloor(arrayBlockValue.meta))
										{
											DebugMsg("Found Floor To Force Bellow Ground (array)");
											BlockEntityData arrayEbcd = _world.ChunkClusters[0].GetBlockEntity(arrayFloor);
											if(arrayEbcd != null && arrayEbcd.bHasTransform)
											{
												DebugMsg("Forcing position (array)");
												liftPlatform.position = new Vector3(liftPlatform.position.x, arrayEbcd.transform.position.y, liftPlatform.position.z);
												return;
											}
										}
									}
								}
							}
						}
					}
					
				}
			}
		}
	}
	
	public static int GetFloorNumber(Vector3i _blockPos)
	{
		WorldBase _world = GameManager.Instance.World;
		BlockValue _blockValue = _world.GetBlock(_blockPos);
		BlockEntityData _ebcd = _world.ChunkClusters[0].GetBlockEntity(_blockPos);
		if(_ebcd != null && _ebcd.bHasTransform)
		{
			string key = _blockPos.x + "_" + _blockPos.z + "_floorsList";
			if(floors.ContainsKey(key))
			{
				List<Vector3i> list = new List<Vector3i>();
				if (floors.TryGetValue(key, out list))
				{
					IEnumerable<Vector3i> sortedList = list.OrderBy(v => v.y);
					Vector3i[] floorVectorArray = sortedList.ToArray();
					foreach(Vector3i arrayFloor in floorVectorArray)
					{
						if(arrayFloor.y == _blockPos.y)
						{
							return System.Array.IndexOf(floorVectorArray, arrayFloor) + 1;
						}
					}
				}
			}
		}
		return 0;
	}
	
	public static void SetAllFalseIsAtThisFloor(Vector3i _blockPos)
	{
		WorldBase _world = GameManager.Instance.World;
		string key = _blockPos.x + "_" + _blockPos.z + "_floorsList";
		if(floors.ContainsKey(key))
		{
			List<Vector3i> list = new List<Vector3i>();
			if (floors.TryGetValue(key, out list))
			{
				IEnumerable<Vector3i> sortedList = list.OrderBy(v => v.y);
				Vector3i[] floorVectorArray = sortedList.ToArray();
				foreach(Vector3i arrayFloor in floorVectorArray)
				{
					if(arrayFloor.y != _blockPos.y)
					{
						BlockValue _blockValue = _world.GetBlock(arrayFloor);
						if(!_blockValue.ischild)
						{
							_blockValue.meta = (byte) (_blockValue.meta & ~(1 << 1));
							_world.SetBlockRPC(arrayFloor, _blockValue);
							// force the shaft doors closed here
							Block block = Block.list[_blockValue.type];
							if(block != null)
							{
								BlockMultiLevelElevatorFloor floorBlock = block as BlockMultiLevelElevatorFloor;
								if(floorBlock != null)
								{
									BlockEntityData _ebcd = _world.ChunkClusters[0].GetBlockEntity(arrayFloor);
									if(_ebcd != null && _ebcd.bHasTransform)
									{
										if(!String.IsNullOrEmpty(BlockMultiLevelElevatorGround.ShaftDoor1Transform(block)))
										{
											Transform leftShaftDoorTransform = BlockMultiLevelElevatorGround.FindTransformNamed(_ebcd, BlockMultiLevelElevatorGround.ShaftDoor1Transform(block));
											if(leftShaftDoorTransform != null && leftShaftDoorTransform != default(Transform))
											{
												leftShaftDoorTransform.localPosition =  floorBlock.leftShaftEndPos;
											}
										}
										if(!String.IsNullOrEmpty(BlockMultiLevelElevatorGround.ShaftDoor2Transform(block)))
										{
											Transform rightShaftDoorTransform = BlockMultiLevelElevatorGround.FindTransformNamed(_ebcd, BlockMultiLevelElevatorGround.ShaftDoor2Transform(block));
											if(rightShaftDoorTransform != null && rightShaftDoorTransform != default(Transform))
											{
												rightShaftDoorTransform.localPosition =  floorBlock.rightShaftEndPos;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	public static Vector3i TargetVector3i(float targetFloorNumber, int _blockPosX, int _blockPosZ, bool useY)
	{
		Vector3i target = default(Vector3i);
		// // // add floors for xz
		string key = _blockPosX + "_" + _blockPosZ + "_floorsList";
		if(!floors.ContainsKey(key))
		{
			floors.Add(key, new List<Vector3i>());
		}
		// // // read floors
		if(floors.ContainsKey(key))
		{
			List<Vector3i> list = new List<Vector3i>();
			if (floors.TryGetValue(key, out list))
			{
				IEnumerable<Vector3i> sortedList = list.OrderBy(v => v.y);
				Vector3i[] floorVectorArray = sortedList.ToArray();
				// Debug.Log("Array Length: " + floorVectorArray.Length);
				foreach(Vector3i arrayFloor in floorVectorArray)
				{
					if(!useY)
					{
						// find by floor number
						int floorNumber = System.Array.IndexOf(floorVectorArray, arrayFloor) + 1;
						// Debug.Log("Floor " + floorNumber + " position: " + arrayFloor);
						if(targetFloorNumber == floorNumber)
						{
							target = arrayFloor;
						}
					}
					else
					{
						// find by y value
						BlockEntityData _ebcd = GameManager.Instance.World.ChunkClusters[0].GetBlockEntity(arrayFloor);
						if(_ebcd != null && _ebcd.bHasTransform)
						{
							DebugMsg("targetFloorNumber: " + targetFloorNumber);
							DebugMsg("targetYvalue: " + _ebcd.transform.position.y);
							if(targetFloorNumber == _ebcd.transform.position.y)
							{
								target = arrayFloor;
								DebugMsg("FOUND TARGET BY Y VALUE");
							}
						}
						
						if(targetFloorNumber == arrayFloor.y)
						{
							target = arrayFloor;
						}
					}
				}
			}
		}
		
		return target;
	}
	
	public override bool CanPlaceBlockAt(WorldBase _world, int _clrIdx, Vector3i _blockPos, BlockValue _blockValue, bool _bOmitCollideCheck)
	{
		string key = _blockPos.x + "_" + _blockPos.z + "_Ground";
		bool canPlaceFloor = true;
		if(BlockMultiLevelElevatorFloor.MaxFloorCount(this) != 0 && BlockMultiLevelElevatorFloor.MaxFloorCount(this) <= FloorCount(_blockPos))
		{
			canPlaceFloor = false;
			ToolTipTextOnClick("Max Floor Count Reached");
		}
		if(BlockMultiLevelElevatorGround.groundFloors.ContainsKey(key))
		{
			ToolTipTextOnClick("Only 1 " + GroundBlockName() + " Is Allowed For Each Elevator Position");
		}
		return _blockPos.y <= 253 && (GameManager.Instance.IsEditMode() || canPlaceFloor && !BlockMultiLevelElevatorGround.groundFloors.ContainsKey(key) && !((World)_world).IsWithinTraderArea(_blockPos)) && (!Block.list[_blockValue.type].isMultiBlock || _blockPos.y + Block.list[_blockValue.type].multiBlockPos.dim.y < 254) && (GameManager.Instance.IsEditMode() || _bOmitCollideCheck || !this.overlapsWithOtherBlock(_world, _clrIdx, _blockPos, _blockValue));
	}
	
	private bool overlapsWithOtherBlock(WorldBase _world, int _clrIdx, Vector3i _blockPos, BlockValue _blockValue)
	{
		if (!this.isMultiBlock)
		{
			int type = _world.GetBlock(_clrIdx, _blockPos).type;
			return type != 0 && !Block.list[type].blockMaterial.IsGroundCover && !Block.list[type].blockMaterial.IsLiquid;
		}
		byte rotation = _blockValue.rotation;
		for (int i = this.multiBlockPos.Length - 1; i >= 0; i--)
		{
			int type2 = _world.GetBlock(_clrIdx, _blockPos + this.multiBlockPos.Get(i, _blockValue.type, (int)rotation)).type;
			if (type2 != 0 && !Block.list[type2].blockMaterial.IsGroundCover && !Block.list[type2].blockMaterial.IsLiquid)
			{
				return true;
			}
		}
		return false;
	}
	
	private void ToolTipText(string str)
    {
		EntityPlayerLocal entity = GameManager.Instance.World.GetPrimaryPlayer();
		DateTime dteNextToolTipDisplayTime = default(DateTime);
        if (DateTime.Now > dteNextToolTipDisplayTime)
        {
            GameManager.ShowTooltip(entity, str);
			dteNextToolTipDisplayTime = DateTime.Now.AddSeconds(3);
        }
    }
	
	public static void DebugMsg(string msg)
	{
		bool showDebugLog = false;
		if(showDebugLog)
		{
			Debug.Log(msg);
		}
	}
	
	public bool CallButtonActivated(WorldBase _world, int _cIdx, Vector3i _blockPos, BlockValue _blockValue, BlockEntityData callFloor_ebcd, bool noPowerRequired)
	{
		BlockEntityData _ebcd = _world.ChunkClusters[_cIdx].GetBlockEntity(_blockPos);
		if(_ebcd != null && _ebcd.bHasTransform)
		{
			Transform elevatorTransform = FindTransformNamed(_ebcd, PlatformBaseTransform(this));
			if(elevatorTransform != null && elevatorTransform != default(Transform))
			{
				if(this.elevatorController == null && !elevatorTransform.gameObject.GetComponent<MultiLevelElevatorController>())
				{
					elevatorTransform.gameObject.AddComponent<MultiLevelElevatorController>();
				}
			}
			this.elevatorController = elevatorTransform.gameObject.GetComponent<MultiLevelElevatorController>();
		}
		if(this.elevatorController == null)
		{
			DebugMsg("BlockActivated: elevatorController NULL");
			return true;
		}
		else
			DebugMsg("BlockActivated: elevatorController FOUND");
			
		if(this.elevatorController.moveElevator)
		{
			ToolTipText("Elevator in use");
			return true;
		}
		Transform liftPlatform = FindTransformNamed(_ebcd, TransformToMove(this));
		if(liftPlatform != null && liftPlatform != default(Transform) && this.elevatorController != null)
		{
			this.elevatorController.liftPlatform = liftPlatform;
			this.elevatorController.blockMultiLevelElevatorGround = this;
			this.elevatorController.groundBlockPos = _blockPos;
			this.elevatorController.groundBlockValue = _blockValue;
			this.elevatorController.ebcd = _ebcd;
			bool needToMove = false;
			if(RequiresPower(this) && !isBlockPoweredUp(_blockPos, _cIdx) && !noPowerRequired)
			{
				ToolTipText(GroundBlockName() + " Needs power");
				return false;
			}
			if(this.elevatorController.liftPlatform.position.y == callFloor_ebcd.transform.position.y)
			{
				// at call floor, do nothing
				ToolTipText("Elevator is already at that floor");
				return false;
			}
			if(this.elevatorController.liftPlatform.position.y != callFloor_ebcd.transform.position.y)
			{
				// Elevator is at another floor, send it to this floor
				needToMove = true;
				this.elevatorController.startPos = this.elevatorController.liftPlatform.position;
				this.elevatorController.endPos = callFloor_ebcd.transform.position;
				this.elevatorController.upDownSpeed = ElevatorUpSpeed(this);this.elevatorController.targetPos3i = TargetVector3i(callFloor_ebcd.transform.position.y, _blockPos.x, _blockPos.z, true);
				if(!this.elevatorController.moveElevator)
					{
						this.elevatorController.startPos3i = TargetVector3i(this.elevatorController.liftPlatform.position.y, _blockPos.x, _blockPos.z, true);
						if(this.elevatorController.startPos3i == default(Vector3i))
						{
							this.elevatorController.startPos3i = _blockPos;
						}
					}
					else
						DebugMsg("startPos3i was left untouched");
				
			}
			if(this.elevatorController.startPos.y == this.elevatorController.endPos.y)
			{
				ToolTipText("Elevator is already at that floor");
			}
			if(needToMove)
			{
				bool goingUp = GoingUp(this.elevatorController.startPos.y, this.elevatorController.endPos.y);
				if(goingUp)
				{
					ToolTipText("Going Up");
						
				}
				else
					ToolTipText("Going Down");
					
				
				
				// closeDoors must be set before moveElevator
				if(!String.IsNullOrEmpty(Door1Transform(this)))
				{
					Transform leftDoorTransform = FindTransformNamed(_ebcd, Door1Transform(this));
					if(leftDoorTransform != null && leftDoorTransform != default(Transform))
					{
						this.elevatorController.hasDoors = !String.IsNullOrEmpty(Door1Transform(this));
						this.elevatorController.twoDoors = !String.IsNullOrEmpty(Door2Transform(this));
						this.elevatorController.doorSpeed = DoorSpeed(this);
						this.elevatorController.leftDoorTransform = leftDoorTransform;
						if(!String.IsNullOrEmpty(Door2Transform(this)))
						{
							Transform rightDoorTransform = FindTransformNamed(_ebcd, Door2Transform(this));
							if(rightDoorTransform != null && rightDoorTransform != default(Transform))
							{
								this.elevatorController.rightDoorTransform = rightDoorTransform;
							}
						}
						this.elevatorController.doorStartTime = Time.time;
						this.elevatorController.closeDoors = true;
					}
				}
				else
				{
					// Vehicle movement not in use yet
					if(this.elevatorController.vehicleEntity != null)
					{
						this.elevatorController.vehicleStartPos = this.elevatorController.vehicleEntity.transform.position;
						float journeyLength = Vector3.Distance(this.elevatorController.endPos, this.elevatorController.startPos);
						this.elevatorController.vehicleEndPos = new Vector3(this.elevatorController.vehicleEntity.transform.position.x, this.elevatorController.vehicleEntity.transform.position.y+journeyLength+3.5f, this.elevatorController.vehicleEntity.transform.position.z);
						this.elevatorController.vehicleStartTime = Time.time;
					}
					
					this.elevatorController.startTime = Time.time;
					this.elevatorController.moveElevator = true;
					StartMotorSound(_ebcd);
					TriggerAllArrows(_world, _blockPos, goingUp, false);
				}
				if(!String.IsNullOrEmpty(BlockMultiLevelElevatorGround.ShaftDoor1Transform(this)) || !String.IsNullOrEmpty(BlockMultiLevelElevatorGround.ShaftDoor2Transform(this)))
				{
					this.elevatorController.twoShaftDoors = !String.IsNullOrEmpty(ShaftDoor2Transform(this));
					this.elevatorController.SetShaftDoorTransforms(this.elevatorController.startPos3i, false);
				}
				
			}
		}
		return true;
	}
	
	public override void OnBlockRemoved(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
	{
		if(!_blockValue.ischild)
		{
			string key = _blockPos.x + "_" + _blockPos.z + "_Ground";
			if(groundFloors.ContainsKey(key))
			{
				groundFloors.Remove(key);
			}
			
			string floorsKey = _blockPos.x + "_" + _blockPos.z + "_floorsList";
			if(BlockMultiLevelElevatorGround.floors.ContainsKey(floorsKey))
			{
				List<Vector3i> list = new List<Vector3i>();
				if (BlockMultiLevelElevatorGround.floors.TryGetValue(floorsKey, out list))
				{
					foreach(Vector3i floor in list.ToList())
					{
						if(floor == _blockPos)
						{
							list.Remove(floor);
						}
					}
				}
			}
		}
		SetAllFalseIsAtThisFloor(_blockPos);
		base.OnBlockRemoved(world, _chunk, _blockPos, _blockValue);
		return;
	}
	
	public string num1 = "";
	public string num2 = "";
	public string num3 = "";
	public string nums = "";
	DateTime keyResetTime = default(DateTime);
	int keyResetDelay = 2;
	
	int SetNumbers(int _buttonNum)
	{
		if(!string.IsNullOrEmpty(num1) && !string.IsNullOrEmpty(num2) && !string.IsNullOrEmpty(num3) || DateTime.Now > keyResetTime)
		{
			num1 = "";
			num2 = "";
			num3 = "";
			nums = "";
		}
		if(string.IsNullOrEmpty(num1))
		{
			num1 = _buttonNum.ToString();
		}
		else
		{
			if(string.IsNullOrEmpty(num2))
			{
				num2 = _buttonNum.ToString();
			}
			else
			{
				if(string.IsNullOrEmpty(num3))
				{
					num3 = _buttonNum.ToString();
				}
			}
		}
		nums = num1+num2+num3;
		int floorNumber = 0;
		bool tempFloorNumber = Int32.TryParse(nums, out floorNumber);
		keyResetTime = DateTime.Now.AddSeconds(keyResetDelay);
		return floorNumber;
	}
	
	public void GetKeyPress(BlockMultiLevelElevatorGround _groundBlock, Vector3i _blockPos)
	{
		
		if (Input.GetKeyUp(KeyCode.R))
		{
			ToolTipText("There are " + FloorCount(_blockPos) + " floors");
		}
		if (Input.GetKeyUp(KeyCode.Keypad1))
		{
			int floorNumber = SetNumbers(1);
			GetSetSelectedFloor(_blockPos, floorNumber, false);
		}
		if (Input.GetKeyUp(KeyCode.Keypad2))
		{
			int floorNumber = SetNumbers(2);
			GetSetSelectedFloor(_blockPos, floorNumber, false);
		}
		if (Input.GetKeyUp(KeyCode.Keypad3))
		{
			int floorNumber = SetNumbers(3);
			GetSetSelectedFloor(_blockPos, floorNumber, false);
		}
		if (Input.GetKeyUp(KeyCode.Keypad4))
		{
			int floorNumber = SetNumbers(4);
			GetSetSelectedFloor(_blockPos, floorNumber, false);
		}
		if (Input.GetKeyUp(KeyCode.Keypad5))
		{
			int floorNumber = SetNumbers(5);
			GetSetSelectedFloor(_blockPos, floorNumber, false);
		}
		if (Input.GetKeyUp(KeyCode.Keypad6))
		{
			int floorNumber = SetNumbers(6);
			GetSetSelectedFloor(_blockPos, floorNumber, false);
		}
		if (Input.GetKeyUp(KeyCode.Keypad7))
		{
			int floorNumber = SetNumbers(7);
			GetSetSelectedFloor(_blockPos, floorNumber, false);
		}
		if (Input.GetKeyUp(KeyCode.Keypad8))
		{
			int floorNumber = SetNumbers(8);
			GetSetSelectedFloor(_blockPos, floorNumber, false);
		}
		if (Input.GetKeyUp(KeyCode.Keypad9))
		{
			int floorNumber = SetNumbers(9);
			GetSetSelectedFloor(_blockPos, floorNumber, false);
		}
		if (Input.GetKeyUp(KeyCode.Keypad0))
		{
			int floorNumber = SetNumbers(0);
			GetSetSelectedFloor(_blockPos, floorNumber, false);
		}
	}
}
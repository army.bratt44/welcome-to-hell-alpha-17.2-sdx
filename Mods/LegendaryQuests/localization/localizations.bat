@echo off
for %%I in (.) do set CurrDirName=%%~nxI
IF NOT EXIST "Localization.txt" goto errorfile
IF NOT EXIST "Localization - Quest.txt" goto errorfile
IF NOT EXIST "..\..\..\data\config\Localization.txt" goto errorlocation
IF NOT EXIST "..\..\..\data\config\Localization - Quest.txt" goto errorlocation
IF EXIST "Localization.txt" goto fileconfirmed
:fileconfirmed
IF EXIST "..\..\..\data\config\Localization.txt" goto confirmed
:errorlocation                                                                                                                                                                                                        
color 0C
echo EEEEEEEEEEEEEEEEEEEEEERRRRRRRRRRRRRRRRR   RRRRRRRRRRRRRRRRR        OOOOOOOOO     RRRRRRRRRRRRRRRRR   
echo E::::::::::::::::::::ER::::::::::::::::R  R::::::::::::::::R     OO:::::::::OO   R::::::::::::::::R  
echo E::::::::::::::::::::ER::::::RRRRRR:::::R R::::::RRRRRR:::::R  OO:::::::::::::OO R::::::RRRRRR:::::R 
echo EE::::::EEEEEEEEE::::ERR:::::R     R:::::RRR:::::R     R:::::RO:::::::OOO:::::::ORR:::::R     R:::::R
echo   E:::::E       EEEEEE  R::::R     R:::::R  R::::R     R:::::RO::::::O   O::::::O  R::::R     R:::::R
echo   E:::::E               R::::R     R:::::R  R::::R     R:::::RO:::::O     O:::::O  R::::R     R:::::R
echo   E::::::EEEEEEEEEE     R::::RRRRRR:::::R   R::::RRRRRR:::::R O:::::O     O:::::O  R::::RRRRRR:::::R 
echo   E:::::::::::::::E     R:::::::::::::RR    R:::::::::::::RR  O:::::O     O:::::O  R:::::::::::::RR  
echo   E:::::::::::::::E     R::::RRRRRR:::::R   R::::RRRRRR:::::R O:::::O     O:::::O  R::::RRRRRR:::::R 
echo   E::::::EEEEEEEEEE     R::::R     R:::::R  R::::R     R:::::RO:::::O     O:::::O  R::::R     R:::::R
echo   E:::::E               R::::R     R:::::R  R::::R     R:::::RO:::::O     O:::::O  R::::R     R:::::R
echo   E:::::E       EEEEEE  R::::R     R:::::R  R::::R     R:::::RO::::::O   O::::::O  R::::R     R:::::R
echo EE::::::EEEEEEEE:::::ERR:::::R     R:::::RRR:::::R     R:::::RO:::::::OOO:::::::ORR:::::R     R:::::R
echo E::::::::::::::::::::ER::::::R     R:::::RR::::::R     R:::::R OO:::::::::::::OO R::::::R     R:::::R
echo E::::::::::::::::::::ER::::::R     R:::::RR::::::R     R:::::R   OO:::::::::OO   R::::::R     R:::::R
echo EEEEEEEEEEEEEEEEEEEEEERRRRRRRR     RRRRRRRRRRRRRRR     RRRRRRR     OOOOOOOOO     RRRRRRRR     RRRRRRR
echo.
echo.
echo It looks like you are trying to run this batch script from the wrong location.
echo Make sure this mod folder is in "7 Days To Die/Mods/".
echo.
echo If this error persists, check that the games localization files are where they should be:
echo "7 Days To Die/Data/Config/"
goto end
:errorfile                                                                                                                                                                                                      
color 0C
echo EEEEEEEEEEEEEEEEEEEEEERRRRRRRRRRRRRRRRR   RRRRRRRRRRRRRRRRR        OOOOOOOOO     RRRRRRRRRRRRRRRRR   
echo E::::::::::::::::::::ER::::::::::::::::R  R::::::::::::::::R     OO:::::::::OO   R::::::::::::::::R  
echo E::::::::::::::::::::ER::::::RRRRRR:::::R R::::::RRRRRR:::::R  OO:::::::::::::OO R::::::RRRRRR:::::R 
echo EE::::::EEEEEEEEE::::ERR:::::R     R:::::RRR:::::R     R:::::RO:::::::OOO:::::::ORR:::::R     R:::::R
echo   E:::::E       EEEEEE  R::::R     R:::::R  R::::R     R:::::RO::::::O   O::::::O  R::::R     R:::::R
echo   E:::::E               R::::R     R:::::R  R::::R     R:::::RO:::::O     O:::::O  R::::R     R:::::R
echo   E::::::EEEEEEEEEE     R::::RRRRRR:::::R   R::::RRRRRR:::::R O:::::O     O:::::O  R::::RRRRRR:::::R 
echo   E:::::::::::::::E     R:::::::::::::RR    R:::::::::::::RR  O:::::O     O:::::O  R:::::::::::::RR  
echo   E:::::::::::::::E     R::::RRRRRR:::::R   R::::RRRRRR:::::R O:::::O     O:::::O  R::::RRRRRR:::::R 
echo   E::::::EEEEEEEEEE     R::::R     R:::::R  R::::R     R:::::RO:::::O     O:::::O  R::::R     R:::::R
echo   E:::::E               R::::R     R:::::R  R::::R     R:::::RO:::::O     O:::::O  R::::R     R:::::R
echo   E:::::E       EEEEEE  R::::R     R:::::R  R::::R     R:::::RO::::::O   O::::::O  R::::R     R:::::R
echo EE::::::EEEEEEEE:::::ERR:::::R     R:::::RRR:::::R     R:::::RO:::::::OOO:::::::ORR:::::R     R:::::R
echo E::::::::::::::::::::ER::::::R     R:::::RR::::::R     R:::::R OO:::::::::::::OO R::::::R     R:::::R
echo E::::::::::::::::::::ER::::::R     R:::::RR::::::R     R:::::R   OO:::::::::OO   R::::::R     R:::::R
echo EEEEEEEEEEEEEEEEEEEEEERRRRRRRR     RRRRRRRRRRRRRRR     RRRRRRR     OOOOOOOOO     RRRRRRRR     RRRRRRR
echo.
echo.
echo Well... it seems we are missing the following files:
echo.
IF NOT EXIST "Localization.txt" echo Localization.txt
IF NOT EXIST "Localization - Quest.txt" echo Localization - Quest.txt
echo.
echo Make sure you have these files in the same place you are running this script.
echo Also make sure you haven't renamed them.
goto end
:confirmed
color 0B                                                                                                         
echo ____     __________   ____   __________ ___      ___________        _     ________  ____     ___ 
echo `MM'     `MMMMMMMMM  6MMMMb/ `MMMMMMMMM `MM\     `M'`MMMMMMMb.     dM.    `MMMMMMMb.`MM(     )M' 
echo  MM       MM      \ 8P    YM  MM      \  MMM\     M  MM    `Mb    ,MMb     MM    `Mb `MM.    d'  
echo  MM       MM       6M      Y  MM         M\MM\    M  MM     MM    d'YM.    MM     MM  `MM.  d'   
echo  MM       MM    ,  MM         MM    ,    M \MM\   M  MM     MM   ,P `Mb    MM     MM   `MM d'    
echo  MM       MMMMMMM  MM         MMMMMMM    M  \MM\  M  MM     MM   d'  YM.   MM    .M9    `MM'     
echo  MM       MM    `  MM     ___ MM    `    M   \MM\ M  MM     MM  ,P   `Mb   MMMMMMM9'     MM      
echo  MM       MM       MM     `M' MM         M    \MM\M  MM     MM  d'    YM.  MM  \M\       MM      
echo  MM       MM       YM      M  MM         M     \MMM  MM     MM ,MMMMMMMMb  MM   \M\      MM      
echo  MM    /  MM      / 8b    d9  MM      /  M      \MM  MM    .M9 d'      YM. MM    \M\     MM      
echo _MMMMMMM _MMMMMMMMM  YMMMM9  _MMMMMMMMM _M_      \M _MMMMMMM9_dM_     _dMM_MM_    \M\_  _MM_                                                                   
echo                      ____   ____     ____________   ____ __________   ____                       
echo                     6MMMMb  `MM'     `M`MMMMMMMMM  6MMMMbMMMMMMMMMM  6MMMMb\                     
echo                   8P    Y8  MM       M MM      \ 6M'    /   MM   \ 6M'    `                     
echo                   6M      Mb MM       M MM        MM         MM     MM                           
echo                   MM      MM MM       M MM    ,   YM.        MM     YM.                          
echo                   MM      MM MM       M MMMMMMM    YMMMMb    MM      YMMMMb                      
echo                   MM      MM MM       M MM    `        `Mb   MM          `Mb                     
echo                   MM      MM MM       M MM              MM   MM           MM                     
echo                   YM      M9 YM       M MM              MM   MM           MM                     
echo                    8b    d8   8b     d8 MM      / L    ,M9   MM     L    ,M9                     
echo                     YMMMM9     YMMMMM9 _MMMMMMMMM MYMMMM9   _MM_    MYMMMM9                      
echo                       MM                                                                         
echo                       YM.                                                                        
echo                        `Mo
echo.
pause
echo.                                                                       
echo.
for %%I in (.) do set CurrDirName=%%~nxI
echo This will add the localization data for "Legendary Quests" to your existing localization files.
echo.
echo This should be compatible with other batch scripts that attempt the same thing.
echo Please note the key word "SHOULD".
echo.
echo This script does make a backup of the current Localization files before it does anything. 
echo.
:start
echo.
echo Please type the number of your choice.
echo 1: Continue
echo 2: Quit
echo 3: Restore a backup if one exists
set /p choice=Option: 
if not '%choice%'=='' set choice=%choice:~0,1%
if '%choice%'=='1' goto install
if '%choice%'=='2' goto end
if '%choice%'=='3' goto restore
ECHO "%choice%" Choice is not valid, try again
ECHO.
goto start
:errorinstalled
echo.
echo This mod's localization has already been added.
goto end
:errorbackup
echo.
echo There is no localization backup to restore.
goto end
:install
echo.
echo DETECTING PREVIOUS INSTALL:
FIND "--TaycottLegendaryQuests" "..\..\..\data\config\Localization.txt"
IF %ERRORLEVEL% equ 0 goto errorinstalled
FIND "--TaycottLegendaryQuests" "..\..\..\data\config\Localization - Quest.txt"
IF %ERRORLEVEL% equ 0 goto errorinstalled
echo.
echo CREATING BACKUP:
echo.
IF EXIST "Localization.txt" copy "..\..\..\data\config\Localization.txt" "..\..\..\data\config\Localization.txt.bak"
IF EXIST "Localization - Quest.txt" copy "..\..\..\data\config\Localization - Quest.txt" "..\..\..\data\config\Localization - Quest.txt.bak"
echo.
echo APPENDING LOCALIZATION:
echo.
type localization.txt >> "../../../data/config/Localization.txt"
type "localization - Quest.txt" >> "../../../data/config/Localization - Quest.txt"
echo Finished
goto end
:restore
IF NOT EXIST ..\..\..\data\config\Localization.txt.bak goto errorbackup
IF NOT EXIST "..\..\..\data\config\Localization - Quest.txt.bak" goto errorbackup
echo.
IF EXIST ..\..\..\data\config\Localization.txt.bak echo RESTORING BACKUP:
echo.
IF EXIST ..\..\..\data\config\Localization.txt.bak copy "..\..\..\data\config\Localization.txt.bak" "..\..\..\data\config\Localization.txt"
IF EXIST "..\..\..\data\config\Localization - Quest.txt.bak" copy "..\..\..\data\config\Localization - Quest.txt.bak" "..\..\..\data\config\Localization - Quest.txt"
IF EXIST ..\..\..\data\config\Localization.txt.bak del "..\..\..\data\config\Localization.txt.bak"
IF EXIST "..\..\..\data\config\Localization - Quest.txt.bak" del "..\..\..\data\config\Localization - Quest.txt.bak"
echo.
echo Finished
:end
echo.
pause

